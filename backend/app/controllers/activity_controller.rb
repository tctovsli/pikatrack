class ActivityController < ApplicationController
    before_action :authenticate_user!, only: [:create, :destroy]

    def index
        @activities = Activity.visible(current_user).where(user_id: params[:id]).where.not(computed_activities: {id: nil}).order(created_at: :desc)
    end

    def show
        @activity = Activity.visible(current_user).where.not(computed_activity: nil).find_by(id: params[:id])
        render json: {error: 'activity still processing'} unless @activity.computed_activity
    end

    def create
        @activity = Activity.create(activity_params.merge({user: current_user}))
        ActivityWorker.perform_async(@activity.id)
        render 'activity/create', formats: [:json]
    end

    def destroy
        activity = Activity.find(params[:id])
        activity.destroy if activity.user_id == current_user.id
    end

    def activity_params
        params.require(:activity).permit(:title, :description, :privacy, :activity_type, :original_activity_log_file)
    end
end

class SectionController < ApplicationController
    before_action :authenticate_user!, only: [:create]

    def show
        @section = Section.find(params[:id])
    end

    def create
        @section = Section.create(section_params.merge({user: current_user}))
        render 'section/create', formats: [:json]
    end

    def section_params
        params.require(:section).permit(:name, :path, :activity_type)
    end
end

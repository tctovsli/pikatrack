class Activity < ApplicationRecord
    belongs_to :user
    has_many :section_efforts
    has_one :computed_activity
    has_one_attached :original_activity_log_file

    enum privacy: [:public_activity, :friends_activity, :private_activity]
    enum activity_type: ACTIVITY_TYPES

    # TODO: Handle friends when friends feature exists
    scope :visible, -> (user_id) { where("user_id = ? OR privacy = ?", user_id, self.privacies[:public_activity])}

    def activity_file_direct_url
        # This hack is used because CORS redirects are broken in firefox. Fix when closed: https://bugzilla.mozilla.org/show_bug.cgi?id=1346749
        ActiveStorage::Current.host = Rails.application.secrets.base_url
        self.original_activity_log_file.blob.service_url
    end
end

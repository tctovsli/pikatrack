object @section
attributes :id, :name, :slope, :distance, :activity_type
node(:path) { RGeo::GeoJSON.encode(@section.path, json_parser: :json) }
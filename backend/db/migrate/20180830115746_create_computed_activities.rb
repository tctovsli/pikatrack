class CreateComputedActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :computed_activities do |t|
      t.timestamp :start_time, null: false
      t.timestamp :end_time, null: false
      t.float :distance, null: false
      t.integer :moving_time, null: false
      t.float :elevation, null: false
      t.references :activity
      t.timestamps
    end
  end

  remove_column :activities, :start_time
  remove_column :activities, :end_time
  remove_column :activities, :distance
  remove_column :activities, :moving_time
  remove_column :activities, :elevation
end
